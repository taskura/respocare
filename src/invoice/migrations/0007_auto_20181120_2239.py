# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-20 22:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0006_auto_20181120_2214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='invoice_from',
            field=models.TextField(default='\n\t<small style="font-size: 13px;font-weight: normal;">(Specialist Respiratory Care)</small><br>\n\t<small style="font-size: 13px;font-weight: normal;color: #347ab4;">\n\t  155 (1st Floor),\n\t  Aziz Medicine & Medical Equipment Market<br>\n\t  Shahbag, Dhaka-1000.<br>\n\t  Mobile: 01712-166060, 01943-795007 <br>\n\t  E-mail: carerespo@gmail.com\n\t</small>\n'),
        ),
    ]
