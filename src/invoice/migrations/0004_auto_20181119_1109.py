# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-19 11:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0003_auto_20181119_1057'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='invoice',
            name='invoice_product',
        ),
        migrations.AddField(
            model_name='invoiceproduct',
            name='invoice',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='invoice.Invoice'),
            preserve_default=False,
        ),
    ]
